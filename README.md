# README #

### What is this repository for? ###

* Resource files for generating peoplesoft data from AD

## Dependencies ##

Before this will run as intended the following must be setup before hand, eventually this will be configured via bootstrap powershell script

* Proper event log created, see last lines of peoplesoft.ps1
* First scheduled task to run peoplesoft.ps1
* Second scheduled task to only run when peoplesoft.ps1 has completed its run.
